//
//  ViewController.swift
//  CocoaImageDownloader
//
//  Created by keros on 2019/08/25.
//  Copyright © 2019 keros. All rights reserved.
//

import Cocoa
import Kingfisher

class ViewController: NSViewController {

    @IBOutlet weak var urlLabel: NSTextField!
    @IBOutlet weak var startIndexLabel: NSTextField!
    
    @IBOutlet weak var folderLabel: NSTextField!
    var desktopURL:URL!
    var prefix:String!
    var retryCount:Int = 0
    var downloadType:Int = 0
    var fileExtension:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func downloadButtonTapped(_ sender: Any) {
        
        self.desktopURL = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask).first!
        
        self.prefix = urlLabel.stringValue;
        self.retryCount = 0
        self.downloadType = 0
        if self.prefix.hasSuffix("jpg") {
            fileExtension = "jpg"
        } else {
            fileExtension = "png"
        }
        let indexStartOfText = self.prefix.startIndex
        let indexEndOfText = self.prefix.index(self.prefix.endIndex, offsetBy: -7)
        let text = self.prefix[indexStartOfText..<indexEndOfText]
        let startNumber:Int = Int(startIndexLabel.stringValue)!
        imageDownload(text:String(text), index: startNumber)
    }
    
    @IBAction func downloadExButtonTapped(_ sender: Any) {
        
        self.desktopURL = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask).first!
        
        self.prefix = urlLabel.stringValue;
        self.retryCount = 0
        self.downloadType = 1
        if self.prefix.hasSuffix("jpg") {
            fileExtension = "jpg"
        } else {
            fileExtension = "png"
        }
        let indexStartOfText = self.prefix.startIndex
        let indexEndOfText = self.prefix.index(self.prefix.endIndex, offsetBy: -5)
        let text = self.prefix[indexStartOfText..<indexEndOfText]
        let startNumber:Int = Int(startIndexLabel.stringValue)!
        imageDownload(text:String(text), index: startNumber)
    }
    
    @IBAction func download000_tapped(_ sender: Any) {
        
        self.desktopURL = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask).first!
        
        self.prefix = urlLabel.stringValue;
        self.retryCount = 0
        self.downloadType = 2
        if self.prefix.hasSuffix("jpg") {
            fileExtension = "jpg"
        } else {
            fileExtension = "png"
        }
        let indexStartOfText = self.prefix.startIndex
        let indexEndOfText = self.prefix.index(self.prefix.endIndex, offsetBy: -8)
        let text = self.prefix[indexStartOfText..<indexEndOfText]
        let startNumber:Int = Int(startIndexLabel.stringValue)!
        imageDownload(text:String(text), index: startNumber)
    }
    
    @IBAction func download_00tapped(_ sender: Any) {
        self.desktopURL = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask).first!
        
        self.prefix = urlLabel.stringValue;
        self.retryCount = 0
        self.downloadType = 3
        if self.prefix.hasSuffix("jpg") {
            fileExtension = "jpg"
        } else {
            fileExtension = "png"
        }
        let indexStartOfText = self.prefix.startIndex
        let indexEndOfText = self.prefix.index(self.prefix.endIndex, offsetBy: -6)
        let text = self.prefix[indexStartOfText..<indexEndOfText]
        let startNumber:Int = Int(startIndexLabel.stringValue)!
        imageDownload(text:String(text), index: startNumber)
    }
    
    func createFolder(text:String)->URL {
        let pictureDir = FileManager.default.urls(for: .picturesDirectory, in: .userDomainMask).first!
        let filePath = pictureDir.appendingPathComponent(text)
        if !FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                NSLog("Couldn't create document directory")
            }
        }
        
        return filePath
    }
    
    func imageDownload(text:String, index:Int) {
        var name = String(format: "%03d.%@", index, self.fileExtension)
        if self.downloadType == 1 {
            name = String(format: "%d.%@", index, self.fileExtension)
        } else if self.downloadType == 2 {
            name = String(format: "%03d_.%@", index, self.fileExtension)
        } else if self.downloadType == 3 {
            name = String(format: "%02d.%@", index, self.fileExtension)
        }
        let mergeString = text+name
        let url = URL(string: mergeString)
        
        let downloader = ImageDownloader.default
        downloader.downloadImage(with: url!) { result in
            switch result {
            case .success(let value):
                let filename = value.url?.lastPathComponent
                let destinationURL = self.desktopURL.appendingPathComponent(filename!)
                let test = value.image.pngWrite(to: destinationURL)
                print("success : \(filename)")
            case .failure(let error):
                self.retryCount+=1
            
                print("fail")
                //print(error)
            }
            
            if self.retryCount<10 {
                self.imageDownload(text:text, index:index+1)
            } else {
                print("finish>>>>")
            }
        }
    }
}
extension NSImage {
    var pngData: Data? {
        guard let tiffRepresentation = tiffRepresentation, let bitmapImage = NSBitmapImageRep(data: tiffRepresentation) else { return nil }
        return bitmapImage.representation(using: .png, properties: [:])
    }
    func pngWrite(to url: URL, options: Data.WritingOptions = .atomic) -> Bool {
        do {
            try pngData?.write(to: url, options: options)
            return true
        } catch {
            print(error)
            return false
        }
    }
}

